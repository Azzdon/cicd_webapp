import subprocess
from flask import Flask, request

app = Flask(__name__)

def clean_up_containers():
    # Identifier tous les conteneurs (y compris ceux arrêtés)
    all_containers = subprocess.check_output(["docker", "ps", "-aq"]).decode().split()
    
    # Filtrer les conteneurs à conserver
    containers_to_keep = []
    for container in all_containers:
        container_image = subprocess.check_output(["docker", "inspect", "--format='{{.Config.Image}}'", container]).decode().strip()
        if "prom/prometheus" in container_image or "grafana/grafana" in container_image or "prom/node-exporter" in container_image:
            containers_to_keep.append(container)
    
    # Supprimer les conteneurs non désirés
    for container in set(all_containers) - set(containers_to_keep):
        subprocess.run(["docker", "stop", container], check=True)
        subprocess.run(["docker", "rm", container], check=True)

def clean_up_images():
    # Supprimer les images non utilisées, à l'exception des images pour les services essentiels
    subprocess.run(["docker", "image", "prune", "-a", "--force", "--filter", "label!=keep"], check=True)

@app.route('/webhook', methods=['POST'])
def webhook():
    if request.method == 'POST':
        # Nettoyer les conteneurs et les images
        clean_up_containers()
        clean_up_images()

        # Tirez la dernière image Docker avec le tag :latest
        subprocess.run(["docker", "pull", "azzdon/cicd_webapp:latest"], check=True)

        # Lancez le nouveau conteneur Docker avec le tag :latest et montez le volume persistant
        subprocess.run([
            "docker", "run", "-d", "-p", "8080:80", "-v", "webapp_data:/usr/share/nginx/html",
            "azzdon/cicd_webapp:latest"
        ], check=True)

        print("Webhook reçu et conteneur mis à jour.")
        return "Webhook traité", 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

